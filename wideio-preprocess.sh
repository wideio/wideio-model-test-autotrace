#!/bin/bash

VERSION=0.31.1

wget -nd http://downloads.sourceforge.net/project/autotrace/AutoTrace/$VERSION/autotrace-$VERSION.tar.gz
tar -xvzf autotrace-$VERSION.tar.gz 
rm autotrace-$VERSION.tar.gz 
test -d plugins || mkdir plugins
mv autotrace-$VERSION plugins/

## the rest of the compilation is automated
