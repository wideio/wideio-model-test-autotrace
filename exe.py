import simplejson, bson, sys,os
import PIL
import PIL.Image
import numpy
import scipy
import scipy.ndimage
import autotrace
import socket
import cStringIO as StringIO
import base64

def all_layer(f,i):
    return numpy.dstack([f(i[:,:,l]) for l in range(i.shape[2])])

def vectorize(request):
  at=autotrace.AutoTrace()
  at.opts.contents.despeckle_level=10
  serialization="json"
  binarize=0
  preblurradius=6
  postblurradius=4
  perlayeradaptivethresradius=10
  perlayeradaptivethresoff=0.5
  
  if (request.has_key("parameters")):
    if (request["parameters"].has_key("despeckle")):
      at.opts.contents.despeckle_level=int(request["parameters"]["despeckle"])    
    if (request["parameters"].has_key("serialization")):
      serialization=request["parameters"]["serialization"]
    if (request["parameters"].has_key("binarize")):
      binarize=int(request["parameters"]["binarize"])
    if (request["parameters"].has_key("perlayeradaptivethresradius")):
      perlayeradaptivethresradius=int(request["parameters"].REQUEST["perlayeradaptivethresradius"])
    if (request["parameters"].has_key("perlayeradaptivethresoff")):
      perlayeradaptivethresradius=int(request["parameters"].REQUEST["perlayeradaptivethresoff"])

  #at.opts.contents.error_threshold=1	
  at.opts.contents.color_count=2
   #             ('error_threshold', at_real),
   #             ('filter_iterations', c_uint),
   #             ('line_reversion_threshold', at_real),
  at.opts.contents.line_threshold=10000000
  
  
  at.opts.contents.corner_surround=4
  at.opts.contents.corner_threshold=100
  at.opts.contents.error_threshold=0.8
  at.opts.contents.filter_iterations=4
  at.opts.contents.line_reversion_threshold=0.01
  at.opts.contents.tangent_surround=3

  if (request.has_key("image_bytes")):
    img=numpy.asarray(PIL.Image.open(StringIO.StringIO(base64.decodestring(request["image_bytes"]))))  
  else:
    img=numpy.asarray(PIL.Image.open(os.path.join("/wideio/requests/",str(request["_id"]),"image.png")))

  print "a",img.shape

  if (binarize!=0):
    xl=(img.mean(axis=2).astype(numpy.uint8)/128).astype(bool).astype(numpy.uint8)*255
  else:
    xl=img
  
  
  #
  # DO THE IMAGE PROCESSING
  #
  xl=scipy.ndimage.gaussian_filter(xl,(preblurradius,preblurradius,0),0)  
  if (perlayeradaptivethresradius>0):
    bxl=scipy.ndimage.gaussian_filter(xl,(perlayeradaptivethresradius,perlayeradaptivethresradius,0),0)
    xl=((bxl+perlayeradaptivethresoff)<xl).astype(numpy.uint8)*255
    xl=xl[:,:,:3]
  xl=all_layer(lambda i:scipy.ndimage.morphology.grey_erosion(i,(5,5)),xl)
  xl=scipy.ndimage.gaussian_filter(xl,(postblurradius,postblurradius,0),0)

  try:
    PIL.Image.fromarray(xl).save("output/result.png") 
  except:
    sys.stderr.write("could not write output image\n")

  try:
    if len(xl.shape)==2 :
       xl=xl.reshape(*(xl.shape+(1,)))
    data=at.trace(xl).__dict__()  

    data['splines']=map(lambda s:{'color':s['color'],'spline':map(lambda p:(p['v'][0],p['v'][2],p['v'][1]),s['spline'])} ,filter( lambda s:s['spline'][0]['degree']==1 and s['open']==0 and (len(s['spline'])>=3) ,data['splines']))
    
    if (serialization=="json"):
        return simplejson.dumps(data)
    elif (serialization=="bson"):
       return bson.BSON.encode(data)
    else:
       return data
  except Exception,e:
    print "EXCEPTION",e
    raise e  

def encode_int(i):
  return chr(i&0xFF)+chr((i>>8)&0xFF)+chr((i>>16)&0xFF)+chr((i>>24)&0xFF)

def decode_int(s):
    return ord(s[0])+(ord(s[1])<<8)+(ord(s[2])<<8)+(ord(s[3])<<8)

if __name__=="__main__":
  mode="request"
  if (sys.argv>1):
    mode=sys.argv[1]
  if mode=="request":
    request=bson.BSON((file("request/current.bson").read())).decode()
    print vectorize(request)
  if mode=="server":
   try:
    port = int (sys.argv[2])
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)    
    s.connect(("127.0.0.1", port))
    server_running=True
    while server_running:
       amount=decode_int(s.recv(4))
       request=bson.BSON(s.recv(amount)).decode()
       print "SERVER PROCESS : GOT REQUEST"
       if request["type"]=="compute":
           reply={"res":vectorize(request)}
       elif request["type"]=="stop_server":
           server_running=False
           reply={"res":"ok"}
       else:
           reply={"res":"error", "error":"unsupported request type : "+ request["type"]}
       reply=bson.BSON.encode(reply)
       print "SERVER PROCESS : SENDING REPLY"
       s.send(encode_int(len(reply)))
       s.send(reply)
   except Exception,e:
       print "SERVER PROCESS ERROR:",e
